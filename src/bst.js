export default class BST {
    constructor() {
        this.root = null;
        this.leftHeight = 0;
        this.rightHeight = 0;
    }

    insertNode(node) {
        this.root = node;
    }

    removeNode(insertedNode) {
        if (this.search(insertedNode) === false) {
            return false;
        } else {
            let currentNode = this.root;    
            while (currentNode) {
                let isLeaf = !currentNode.left && !currentNode.right;
                if (currentNode.data === insertedNode) {
                    if (isLeaf) { // no children
                      currentNode.data = null;
                      return currentNode.data;
                    } else if (!currentNode.left && currentNode.right) { // right child
                      Object.assign(currentNode, {
                        data: currentNode.right.data,
                        left: currentNode.right.left,
                        right: currentNode.right.right
                      });
                      return null;
                    } else if (currentNode.left && !currentNode.right) { // left child
                      Object.assign(currentNode, {
                        data: currentNode.left.data,
                        left: currentNode.left.left,
                        right: currentNode.left.right
                      });
                      return null;
                    } else if (currentNode.left && currentNode.right) { // two children
                      Object.assign(currentNode, {
                        data: currentNode.left.data,
                        left: null,
                        right: currentNode.right
                      });
                      return null;
                    }
                }
                if (currentNode.data > insertedNode) {
                  currentNode = currentNode.left;
                } else if (currentNode.data < insertedNode) {
                  currentNode = currentNode.right;
                } else {
                  return false;
                }
            }
        }
    }

    checkIfBalanced() {
      const diff = this.leftHeight > this.rightHeight ? this.leftHeight - this.rightHeight : this.rightHeight - this.leftHeight;
      return diff < 2;
    }

    search(value) {
        if (this.root.data === value) {
          return true;
        } else {
          let currentNode = this.root;
          while (true) {
            if (currentNode.data === value) {
              return true;
            } else if (currentNode.data > value) {
              currentNode = currentNode.left;
            } else if (currentNode.data < value) {
                currentNode = currentNode.right;
            }
            if (currentNode === null) {
                return false;
            }
          }
        }
      }

      updateLeftOrRight(insertedNode, tempHeight) {
        if (insertedNode.data < this.root.data && (tempHeight - this.leftHeight) > 0) {
          this.leftHeight = tempHeight;
        } else if (insertedNode.data > this.root.data && (tempHeight - this.rightHeight) > 0) {
          this.rightHeight = tempHeight;
        }
      }

    insert(insertedNode) {
        if (this.root === null) {
          this.root = insertedNode;
        } else {
          let currentNode = this.root; 
          let tempHeight = 0;
          while (true) {
            if (currentNode.data > insertedNode.data) {
              if (currentNode.left === null) {
                currentNode.left = insertedNode;
                this.updateLeftOrRight(insertedNode, tempHeight);
                return this;
              } else {
                currentNode = currentNode.left;
                tempHeight += 1;
              }
            } else if (currentNode.data < insertedNode.data) {
              if (currentNode.right === null) {
                currentNode.right = insertedNode;
                this.updateLeftOrRight(insertedNode, tempHeight);
                return this;
              } else {
                currentNode = currentNode.right;
                tempHeight += 1;
              }
            } else {
                return this;
            }
          }
        }
      }
}