import BST from '../src/bst.js';
import BSTNode from '../src/bst-node.js';

describe.skip('binarySearchTree', () => {

 const pbst = new BST();
  beforeEach(() => {
    pbst.insert(new BSTNode(50));
    pbst.insert(new BSTNode(30));
    pbst.insert(new BSTNode(80));
    pbst.insert(new BSTNode(10));
    pbst.insert(new BSTNode(40));
    pbst.insert(new BSTNode(70));
    pbst.insert(new BSTNode(100));
    pbst.insert(new BSTNode(120));
    pbst.insert(new BSTNode(90));
    pbst.insert(new BSTNode(35));
    pbst.insert(new BSTNode(20));
    pbst.insert(new BSTNode(60));
    pbst.insert(new BSTNode(75));
  });

  test('it should return true if the root node is equal to 50', () => {
    expect(pbst.search(50)).toEqual(true);
  });

  test('it should return false if the root node is not equal to 31', () => {
    expect(pbst.search(31)).toEqual(false);
  });

  test('it should return true if the first child node on the left is equal to 30', () => {
    expect(pbst.search(30)).toEqual(true);
  });

  test('it should return true if the tree includes 70', () => {
    expect(pbst.search(70)).toEqual(true);
  });

  test('it should return false if the node to be removed doesnt exist', () => {
      expect(pbst.removeNode(13)).toEqual(false);
  });

  test('it should remove a node with one left child', () => {
    expect(pbst.root.left.right.data).toEqual(40);
    pbst.removeNode(40);
    expect(pbst.search(40)).toEqual(false);
    expect(pbst.root.left.right.data).toEqual(35);
  });

  test('it should remove a node with one right child', () => {
    expect(pbst.root.left.left.data).toEqual(10);
    pbst.removeNode(10);
    expect(pbst.search(10)).toEqual(false);
    expect(pbst.root.left.left.data).toEqual(20);
  });

  test('it should remove a leaf node', () => {
    pbst.removeNode(20);
    expect(pbst.search(20)).toEqual(false);
  });

  test('it should remove a node with two children', () => {
    pbst.removeNode(70);
    expect(pbst.search(70)).toEqual(false);
  });

  test('it should return true if the tree is balanced', () => {
    expect(pbst.checkIfBalanced()).toEqual(true);
  });

  test('it should return false if the tree is unbalanced', () => {
    pbst.insert(new BSTNode(130));
    pbst.insert(new BSTNode(135));
    expect(pbst.checkIfBalanced()).toEqual(false);
  });
});